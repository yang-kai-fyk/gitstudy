package com.example.gitstudy1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gitstudy1Application {

    public static void main(String[] args) {
        SpringApplication.run(Gitstudy1Application.class, args);
    }

}
